<h1>Planifier un rendez-vous rapidement avec <span class="violet">Frama</span><span class="vert">date</span></h1>
<h2 class="h4"><span class="rouge">Un service libre respectueux des données privées, fourni par</span> <a href="http://framasoft.org"><span class="violet">Frama</span><span class="orange">soft</span></a></h2>

<div class="row">
        <div class="col-sm-8">
                <img class="img-thumbnail" src="img/1.png" alt="Etape 0">
        </div>
        <div class="col-sm-4">
                <p><button class="btn btn-default" type="button"><span class="badge">0.</span> Ouverture</button></p>
                <p>Pour créer un nouveau sondage, </p>
                <p>Rendez-vous sur <a href="http://framadate.org">http://framadate.org</a></p>
                <p>Puis cliquez sur <button class="btn btn-primary" type="button">Créer un sondage spécial dates</button></p>
                <p class="text-info"> Notez que vous pourrez retrouver tous vos sondages en cliquant sur «&nbsp;Où sont mes sondages&nbsp;». Il suffira alors de renseigner votre adresse courriel.</p>
        </div>
</div>
<div class="row">
        <div class="col-sm-8">
                <img class="img-thumbnail" src="img/2.png" alt="Etape 1">
        </div>
        <div class="col-sm-4">
                <p><button class="btn btn-default" type="button"><span class="badge">1.</span> Configuration</button></p>
                <p>Remplissez le formulaire de présentation du sondage, </p>
                <p class="text-info">Votre adresse courriel ne sera utilisée que pour communiquer les informations relatives au sondage (conformément à <a href="http://degooglisons-internet.org/nav/html/charte.html">la charte des services Framasoft</a>).</p>
                <p><strong>Cases à cocher&nbsp;:</strong> choisissez parmi les options proposées. Astuce&nbsp;: recevoir un  courriel à chaque fois qu'un sondé remplit une ligne est utile pour  suivre l'avancée du sondage.</p>
        </div>
</div>
<div class="row">
        <div class="col-sm-8">
                <img class="img-thumbnail" src="img/3.png" alt="Etape 2">
        </div>
        <div class="col-sm-4">
                <p>Définissez les dates du sondage en utilisant le menu contextuel affichant un calendrier, puis affinez vos dates en entrant des horaires. Les boutons + et – vous permettent d'ajouter autant de champs que nécessaire.</p>
                <p class="text-info">Notez le bouton suivant qui, une fois définis les horaires pour une première date, vous permet de cloner ces horaires, si besoin.</p>
                <img class="img-thumbnail" src="img/4.png" alt="Etape bouton clone horaires">
        </div>
</div>
<div class="row">
        <div class="col-sm-8">
                <img class="img-thumbnail" src="img/5.png" alt="Etape 2">
        </div>
        <div class="col-sm-4">
                <p>Dans le récapitulatif de votre sondage, vous pouvez préciser vous-même une date de suppression automatique du sondage.</p>
                <p>Vous pouvez alors <strong>valider</strong> la création de votre sondage</p>
        </div>
</div>
<div class="row">
        <div class="col-sm-8">
                <img class="img-thumbnail" src="img/6.png" alt="Etape 2">
        </div>
        <div class="col-sm-4">
                <p><button class="btn btn-default" type="button"><span class="badge">2.</span> Administration</button></p>
                <p>Après la création du sondage, vous accédez directement à son interface d'administration. Ces données vous sont envoyées par courriel avec 2 adresses :</p>
                <ul>
                        <li><strong>le lien public du sondage</strong> à faire parvenir à vos correspondants,</li>
                        <li><strong>le lien d'administration</strong> à conserver pendant la durée du sondage.</li>
                </ul>
                        <p>Si besoin, vous pouvez d’ores et déjà accomplir votre propre participation au sondage ou indiquer vous-même la participation d'une personne.</p>
                        <p>Notez que depuis l'interface d'administration vous pourrez <strong>exporter</strong> les résultats du sondage en .csv (à ouvrir avec un tableur comme Excel ou LibreOffice Calc). Pour cela, cliquez sur le bouton <button class="btn btn-default" type="button">Export en CSV</button></p>
        </div>
</div>
<div class="row">
        <div class="col-sm-8">
                <img class="img-thumbnail" src="img/7.png" alt="Etape 2">
        </div>
        <div class="col-sm-4">
                <p><button class="btn btn-default" type="button"><span class="badge">3.</span> Procédure de votation</button></p>
                <p><span class="label label-danger">a.</span> Indiquer son nom.</p>
                <p><span class="label label-danger">b.</span> Pour chaque champ, choisir «&nbsp;oui&nbsp;», «&nbsp;si nécessaire&nbsp;» ou «&nbsp;non&nbsp;» (par défaut).</p>
                <p><span class="label label-danger">c.</span> Enregistrer ses choix.</p>
                <p><span class="label label-danger">d.</span> Il est possible de visualiser les résultats du sondage sous forme graphique.</p>
                <p><span class="label label-danger">e.</span> À chaque vote validé, les résultats du sondage sont actualisés.</p>
        </div>
</div>
<div class="row">
        <div class="col-sm-8">
                <img class="img-thumbnail" src="img/Logo_Licence_Art_Libre.png" width="20%" alt="LAL">
                <p class="text-muted">Auteur : Christophe Masutti, Framasoft, juin 2015</p>
                <p class="text-muted">Ce tutoriel est placé sous <a href="http://artlibre.org/licence/lal/">Licence Art Libre</a>.</p>
        </div>
        <div class="col-sm-4">
        </div>
</div>
