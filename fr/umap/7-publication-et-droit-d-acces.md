# Je publie ma carte et en contrôle l'accès

## Ce que nous allons apprendre

*  Insérer une carte dans une page HTML
*  Publier une carte sur Wordpress
*  Adapter les fonctionnalités de la carte
*  Définir qui peut voir ou modifier la carte

## Procédons par étapes

### 1. Insérer une carte dans une page HTML

![](images/umap_share.png) Nous avons vu dans le tutoriel [Je consulte une carte uMap](1-consulter.html) que le menu de partage permet d'*embarquer une carte en iframe*, sans donner plus de détail. Voyons comment cela se passe.

Une **iframe** est une balise du langage informatique HTML qui permet d'intégrer (embarquer) le contenu d'une page Web dans une autre page Web. C'est en fait très simple et nous avons déjà utilisé ce mécanisme pour intégrer une vidéo dans le tutoriel [Je crée des infobulles multimédia](umap/5_-_je_cree_des_infobulles_multimedia).

![](images/export-iframe.png)

Voici les étapes à suivre :

 1.  ouvrir le panneau **Exporter et partager la carte**
 2.  copier la totalité du texte sous **Intégrer la carte dans une iframe** (astuce: placer le curseur sur le texte puis utiliser les raccourcis claiver Ctrl+a pour tout sélectionner puis Ctrl+c pour copier la sélection)
 3.  coller le texte copié dans le code source du fichier HTML dans lequel vous souhaitez intégrer la carte (raccourci clavier: Ctrl+v)

Voici un exemple minimaliste de fichier HTML dans lequel l'iframe d'une carte uMap à été intégrée :

<pre><code>&lt;!DOCTYPE html&gt;
&lt;head&gt;
    &lt;title&gt;Exemple de carte uMap intégrée à une page Web&lt;/title&gt;
    &lt;meta charset="UTF-8"&gt;
&lt;/head&gt;
&lt;body&gt;
    &lt;div&gt;
        &lt;h1&gt;La carte du festival&lt;/h1&gt;
        &lt;iframe width="100%" height="300px" frameBorder="0" src="https://umap.openstreetmap.fr/fr/map/festival-des-3-continents_26381?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=caption&captionBar=false"&gt;&lt;/iframe&gt;
        &lt;p&gt;&lt;a href="https://umap.openstreetmap.fr/fr/map/festival-des-3-continents_26381"&gt;Voir en plein écran&lt;/a&gt;&lt;/p&gt;
        &lt;p&gt;Cette carte vous est proposée par Carto'Cité :-)&lt;/p&gt;
    &lt;/div&gt;
&lt;/body&gt;</code></pre>

Voici la carte intégrée sur cette page, en utilisant les options d'export par défaut :


<pre><code>  &lt;div style="border: solid 1px;"&gt;
    &lt;h1&gt;La carte du festival&lt;/h1&gt;
    &lt;iframe width="100%" height="300px" frameBorder="0" src="https://umap.openstreetmap.fr/fr/map/festival-des-3-continents_26381?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=caption&captionBar=false"&gt;&lt;/iframe&gt;
    &lt;p&gt;&lt;a href="https://umap.openstreetmap.fr/fr/map/festival-des-3-continents_26381"&gt;Voir en plein écran&lt;/a&gt;&lt;/p&gt;
    &lt;p&gt;Cette carte vous est proposée par Carto'Cité :-)&lt;/p&gt;
  &lt;/div&gt;</code></pre>

Bien entendu cela suppose de connaître un peu HTML et de disposer d'un serveur sur lequel publier un tel fichier. Mais le principe est posé et pas si compliqué. Voyons maintenant un cas de figure plus courant.

### 2. Publier une carte sur WordPress

Publier une carte sur un site WordPress se passe de la même façon que ci-dessus, en copiant le *code HTML de l'iframe* dans l'éditeur WordPress. Il est par contre nécessaire d'**utiliser l'éditeur textuel** (onglet Texte) et non l'éditeur visuel.

![](images/import-iframe-wordpress.png)

Publiez la page et le tour est joué !

<p class="alert alert-info">
Pour des raisons de sécurité, les sites mutualisés comme https://fr.wordpress.com/ n'autorisent pas l'inclusion d'iframe. Il vous sera donc impossible de publier une carte uMap sur de tels sites.
</p>

### 3. Adapter les fonctionnalités de la carte

La carte intégrée ci-dessus n'est pas très pratique : sa hauteur est insuffisante et le panneau latéral est partiellement visible. Les boutons disponibles à gauche ne sont pas forcément adaptés, par exemple nous ne souhaitons pas intégrer le sélecteur de calques.

L'onglet **Options d'export de l'iframe** permet de contrôler tout cela. Certaines de ces options correspondent aux **Options d'interface** vu dans le tutoriel [Je modifie et personnalise ma carte](4-personnaliser.html). Il suffit d'activer ces options pour que le *code d'import de l'iframe* soit modifié. Une fois les options choisies, copiez ce code puis intégrez-le dans celui votre page Web.

![](images/options-export-iframe.png)

Les premières options sont spécifiques à l'export par iframe et méritent d'être commentées :

*  la **largeur** de 100% permet d'utiliser toute la largeur disponible de la page. Vous pouvez définir une largeur fixe en remplaçant le texte par une largeur en pixels, par exemple ''800px''
*  le **lien "plein écran"** désigne le lien ''Voir en plein écran'' placé sous la carte. Celui-ci permet à l'utilisateur d'afficher la carte uMap telle nous l'avons vue jusqu'ici.
*  l'option **Vue courante plutôt que vue par défaut** permet d'appliquer le position et le niveau de zoom actuel de la carte à l'export. Cette option est par exemple intéressante pour produire plusieurs zooms d'une même carte.
*  l'option **Garder les calques visibles actuellement** permet de choisir les calques inclus dans la carte exportée. Cette option est utile pour produire plusieurs cartes pour plusieurs profils d'utilisateurs.
*  **Autoriser le zoom avec la molette** est peu adapté si la carte est intégrée dans une longue page, que les utilisateurs vont faire défiler avec la molette : arrivé à la carte la page ne défilera plus et la carte va effectuer un zoom arrière. Rien de grave mais ce comportement peut être surprenant.

<p class="alert alert-info">
Lorsque les options **Vue courante plutôt que vue par défaut** et **Garder les calques visibles actuellement** sont actives, modifier la vue courante ou les calques visibles ne modifie pas le code d'export. Vous devez désactiver puis réactiver l'option pour prendre en compte ces modifications.
</p>

Voici par exemple la même carte que ci-dessus, avec une vue et un choix de calque différents, et la plupart des options désactivées. Il est possible de déplacer la carte mais pas de zoomer ni modifier les calques.

<iframe width="600px" height="400px" frameBorder="0" src="https://umap.openstreetmap.fr/fr/map/festival-des-3-continents_26381?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=null&allowEdit=false&moreControl=false&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=false&onLoadPanel=none&captionBar=false&datalayers=53329%2C53328&locateControl=null&fullscreenControl=false#15/47.2132/-1.5503"></iframe>

### 4. Définir qui peut voir ou modifier la carte

![](images/umap_edit_rights.png)
Le bouton **Changer les permissions et les éditeurs** donne accès au panneau **Permissions de la carte**. Celui-ci vous permet de contrôler, pour chaque carte, qui peut la voir et qui peut la modifier.

![](images/permissions.png)

<p class="alert alert-info">
Toute modification dans ce panneau doit être confirmée en cliquant sur <b>Envoyer</b> en bas du panneau, et non sur Enregistrer comme nous l'avons fait jusqu'ici.
</p>

Lorsque vous créez une carte celle-ci est visible dans votre *catalogue* de cartes, dont l'adresse est
<pre><code>https://umap.openstreetmap.fr/fr/user/&lt;votre-compte&gt;/</code></pre>

L'option **Tout le monde (public)** du menu déroulant **Qui a accès** est sélectionnée. Les autres options de ce menu sont :

*  **quiconque a le lien** : la carte n'apparaît plus dans votre catalogue mais les personnes connaissant son lien peuvent la consulter.
*  **seulement les éditeurs** : seules les personnes ayant le droit d'éditer la carte, et identifiées comme telles, peuvent consulter la carte. Toute autre personne se verra refuser l'accès. N'utilisez pas cette option si vous intégrez la carte dans une iframe.

Lorsque vous créez une carte, vous êtes le seul à pouvoir la modifier. Vous pouvez inviter d'autres utilisateurs à la modifier en sélectionnant l'option **Seuls les éditeurs peuvent éditer** dans le menu **Statut d'édition**, puis en saisissant un à un le nom de compte des utilisateurs invités dans le champ **Éditeurs**.
Le nom de chaque utilisateur est ajouté à la suite de ce champ.

L'option **Tout le monde peut éditer** du menu **Statut d'édition** est utile pour créer une carte collectivement.

<p class="alert alert-info">
uMap ne permet pas à plusieurs éditeurs de modifier la carte simultanément. Le logiciel vous alerte lorsque l'opération <b>Enregistrer</b> risque d'écraser les modifications d'un autre utilisateur, vous devez alors choisir entre ses modifications (en validant Enregistrer) ou les vôtres (en annulant).
Si vous êtes plusieurs éditeurs d'une même carte, concertez-vous avant de modifier la carte.
</p>

Enfin vous pouvez **tranférer la propriété** d'une carte à un autre utilisateur : supprimez le propriétaire actuel (vous) en cliquant sur la petite croix à droite du champ **Owner**, puis saisissez le nom de compte de l'utilisateur à qui vous donnez la carte.

## Faisons le point

À ce stade nous savons créer une carte structurée avec du contenu multimédia, nous savons la publier et l'intégrer à une page Web, nous savons même la modifier collectivement. Nous allons bientôt pouvoir passer au niveau avancé, dans lequel nous allons apprendre à **importer des données** dans une carte et explorer les capacités d'ouverture de uMap.

Mais avant cela, nous allons terminer le niveau intermédiaire en traitant [Le cas des polygones](8-polygones.html).

